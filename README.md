### Use Case Video
[Sac County Road Rewards](https://youtu.be/6vSytgIUGVI)
### Beacon detected on road

![alt tag](Images/offer2.gif)

### Merchant creates promo from phone app
![alt tag] (Images/Rewards.gif)

###Beacon database integrated with County Speed Limits
![alt tag](Images/BeacondB.gif)

## Project Information
This App aims to connect Sacramento County, the public and local businesses on the streets of Sacramento County.

The App rewards users for simply traveling the 5000+ miles of roads of Sacramento County while following posted speed limits.

The App provides a new proximity advertising channel to locally relevant businesses.

The App provides Virtual Billboard rental revenue to the County from businesses that chose to participate.

Additionally, the County also gains valuable, anonyomous road usage information from the App users. 

Ad quality is controlled by the public through a Tinder like Ad selection system (junk ads/businesses are quickly blocked out through social selection) to prevent ad spam.

Anonymous, type of vehicle data (SUV, Bike, On Foot, Truck) is provided to the County without collecting any private information from the APp user. The County can utilize the data to better plan roads/ traffic flows. 

### Project Name
Sac County Road Rewards

### Project Description
The App uses low power Bluetooth Beacon technology to enable this innovative use case. Beacons placed are regular intervals on posted speed limits are detected by the App. The vehicle speed is calculated thruough a combination of GPS & Beacon signals. Ads are delivered to App users that follow posted speed limits. Each Beacon is tied to the the speed limit posted at the particular location/ road. The Beacons are also tied in to Businesses in close proximity of the Beacon.

The App uses 2 major data sets from the County of Sacramento :
1. Posted Speed Limits 
2. Active Businesses registered with the County (20k+ registered businesses)

We converted raw data into App/mobile friendly real time data base. 
We also augmented the Speed Limit data and converted it to geo hashes mapped to intersections where speed limits are posted. We also assigned Unique Beacon IDs to the various posted speed limit locations which allows us to tie in .

App users use wither Android or ios App.

Businesses use an app to post ads to their beacons. 

The County uses the back end databases to update data.

3. ### API Demo: Business Licenses
https://speedlimitmerchants.firebaseio.com/merchantDetails.json?orderBy=%22Business_Name%22&startAt=%22A%22&endAt=%22B%22&limitToFirst=5

### API Demo: Speed Limits
https://speedlimitmerchants.firebaseio.com/merchantDetails.json?orderBy=%22Business_Name%22&startAt=%22A%22&endAt=%22B%22&limitToFirst=5

### Team Members
Nilesh Shah - eshnil2000@gmail.com
Samaira Shah - sssportstar@gmail.com

### Stakeholder Engagement
We reviewed the concept with 
###Reza Moghissi, Chief Transportation Department (SacDOT) 
and his team at 4100 Traffic Way, Sacramento, CA 95827. 

They vetted and cleared major items for the value proposition of this App:
###1. We got the OK to mount Beacons on Sac County speed limit signs.

###2. The SacDOT confirmed they would find the vehicle road usage data (anonymous) extremely valuable for strategic road planning traffic flow planning. Today, they employ a spotter to watch live video mounted on roads and manually count/estimate traffic.  

###3. The Transportation Department is in need of close to $500M to repair the 5000+ miles of Sacramento County Roads, additional revenue from ads would be most welcome to benefit the road maintenance effort.

### Developer Documentation
The App is Hybrid technology using Cordova. the same code base is used for IOS & Android App.
to build Android App:
Cordova build -debug android

to build IOS App:
Cordova build -debug ios

Beacons:
Any low power bluetooth beacon can be used. We used the following beacons:
https://punchthrough.com/bean
DOcumentation to program the beacons can be found above.

Primarily, we used only the Low Power Blueetooth portion of the beacon and powered off everything else. 
The beacons can last from upto 3 years. 

We chose Low Power Bluetooth is attractive for phones & mobile devices like watches, because only a fraction of GPS power is utilized without noticeable drain on the battery.

The APIs are documented above.
